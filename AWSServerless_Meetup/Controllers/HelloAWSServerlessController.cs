﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace AWSServerless_Meetup.Controllers
{
    public class HelloAWSServerlessController : Controller
    {
        [Route("api/[controller]")]

        // GET api/helloawsserverless
        [HttpGet]
        public IActionResult Get()
        {
            return Ok("Hello AWS Serverless meetup Auckland");
        }
    }
}